<!--

These variables are read and used by XML files; they are included
separate from the main document since they may need to change.  This
allows builders to override the defaults without editing the main XML
file.

These entries are language independent

Remember, this is XML; the *first* definition of an ENTITY wins.

-->

<!-- proper (long) name to use for Debian -->
<!ENTITY debian "Debian">

<!-- release data -->
<!ENTITY release "8">
<!ENTITY releasename "jessie">
<!ENTITY Releasename "Jessie">
<!ENTITY oldrelease "7">
<!ENTITY oldreleasename "wheezy">
<!ENTITY Oldreleasename "Wheezy">
<!ENTITY nextrelease "9">
<!ENTITY nextreleasename "stretch">
<!ENTITY Nextreleasename "Stretch">
<!-- These values can be obtained using the changes-release.pl script -->
<!ENTITY packages-new "12253">
<!ENTITY packages-total "43512">
<!ENTITY packages-updated "24573">
<!ENTITY packages-update-percent "66">
<!ENTITY packages-removed "5441">
<!ENTITY packages-removed-percent "14">

<!-- URLs -->
<!ENTITY url-release-notes "https://www.debian.org/releases/jessie/releasenotes">
<!ENTITY url-svn-release-notes "https://anonscm.debian.org/viewvc/ddp/manuals/trunk/release-notes/">
<!-- TODO: The url-installer links should point to the current status (testing/stable) of the release notes, not just directly to stable -->
<!ENTITY url-installer "https://www.debian.org/releases/jessie/debian-installer/">
<!ENTITY url-installer-errata "https://www.debian.org/devel/debian-installer/errata">
<!ENTITY url-installer-news "https://www.debian.org/devel/debian-installer/News/">
<!ENTITY url-install-manual "https://www.debian.org/releases/wheezy/installmanual">
<!ENTITY url-bts "https://bugs.debian.org/">
<!ENTITY url-bts-rn "https://bugs.debian.org/release-notes">
<!ENTITY url-debian-i18n "https://www.debian.org/international/">
<!ENTITY url-debian-list-archives "https://lists.debian.org/">
<!ENTITY url-debian-mirrors "https://www.debian.org/distrib/ftplist">
<!-- an example mirror w/o trailing slash -->
<!ENTITY url-debian-mirror-eg "http://mirrors.kernel.org">
<!ENTITY url-irc-host "http://www.oftc.net/">
<!ENTITY debian-irc-server "irc.debian.org">
<!ENTITY url-ports "https://www.debian.org/ports/">
<!ENTITY url-wnpp "https://www.debian.org/devel/wnpp/">
<!ENTITY url-ddp "https://www.debian.org/doc/">
<!ENTITY url-debian-projects "https://www.debian.org/devel/#projects">
<!ENTITY url-debian-blends "https://wiki.debian.org/DebianPureBlends">
<!ENTITY url-debian-jr "https://www.debian.org/devel/debian-jr/">
<!ENTITY url-debian-med "https://www.debian.org/devel/debian-med/">
<!ENTITY url-apt-pin-howto "https://www.debian.org/doc/manuals/apt-howto/ch-apt-get">
<!ENTITY url-securing-debian "https://www.debian.org/doc/manuals/securing-debian-howto/">
<!ENTITY url-debian-backports "http://backports.debian.org/">
<!ENTITY url-ddp-svn-info "https://www.debian.org/doc/cvs">
<!ENTITY url-wiki "https://wiki.debian.org/">
<!ENTITY url-wiki-selinux "&url-wiki;SELinux">
<!ENTITY url-wiki-newinlenny "&url-wiki;NewInLenny">
<!ENTITY url-wiki-newinsqueeze "&url-wiki;NewInSqueeze">
<!ENTITY url-wiki-newinwheezy "&url-wiki;NewInWheezy">
<!ENTITY url-wiki-newinjessie "&url-wiki;NewInJessie">
<!ENTITY url-ftpmaster "https://ftp-master.debian.org">

<!ENTITY architecture "<phrase arch='amd64'>amd64</phrase><!--
		    --><phrase arch='arm64'>arm64</phrase><!--
		    --><phrase arch='armel'>armel</phrase><!--
		    --><phrase arch='armhf'>armhf</phrase><!--
		    --><phrase arch='i386'>i386</phrase><!--
		    --><phrase arch='mips'>mips</phrase><!--
		    --><phrase arch='mipsel'>mipsel</phrase><!--
		    --><phrase arch='powerpc'>powerpc</phrase><!--
		    --><phrase arch='ppc64el'>ppc64el</phrase><!--
		    --><phrase arch='s390'>s390</phrase><!--
		    --><phrase arch='s390x'>s390x</phrase>">

<!-- proper nouns for architectures -->
<!ENTITY arch-title "<phrase arch='amd64'>64-bit PC</phrase><!--
		  --><phrase arch='arm64'>64-bit ARM</phrase><!--
		  --><phrase arch='armel'>ARM EABI</phrase><!--
		  --><phrase arch='armhf'>ARMv7 (EABI hard-float ABI)</phrase><!--
		  --><phrase arch='i386'>32-bit PC</phrase><!--
		  --><phrase arch='mips'>Mips</phrase><!--
		  --><phrase arch='mipsel'>Mipsel</phrase><!--
		  --><phrase arch='powerpc'>PowerPC</phrase><!--
		  --><phrase arch='ppc64el'>64-bit little-endian PowerPC</phrase><!--
		  --><phrase arch='s390'>S/390</phrase><!--
		  --><phrase arch='s390x'>IBM System z</phrase>">

<!-- default kernel version, taken from d-i... -->
<!ENTITY kernelversion "<phrase arch='amd64'>3.16</phrase><!--
                     --><phrase arch='arm64'>3.16</phrase><!--
                     --><phrase arch='armel'>3.16</phrase><!--
                     --><phrase arch='armhf'>3.16</phrase><!--
                     --><phrase arch='i386'>3.16</phrase><!--
                     --><phrase arch='mips'>3.16</phrase><!--
                     --><phrase arch='mipsel'>3.16</phrase><!--
                     --><phrase arch='powerpc'>3.16</phrase><!--
                     --><phrase arch='ppc64el'>3.16</phrase><!--
                     --><phrase arch='s390'>3.16</phrase><!--
                     --><phrase arch='s390x'>3.16</phrase>">
